/*
 * SEMIDRIVE Copyright Statement
 * Copyright (c) SEMIDRIVE. All rights reserved
 *
 * This software and all rights therein are owned by SEMIDRIVE, and are
 * protected by copyright law and other relevant laws, regulations and
 * protection. Without SEMIDRIVE's prior written consent and/or related rights,
 * please do not use this software or any potion thereof in any form or by any
 * means. You may not reproduce, modify or distribute this software except in
 * compliance with the License. Unless required by applicable law or agreed to
 * in writing, software distributed under the License is distributed on
 * an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND, either express or implied.
 *
 * You should have received a copy of the License along with this program.
 * If not, see <http://www.semidrive.com/licenses/>.
 */


/* include local headers */
#include "rtos_rpmsg_api.h"
#include "rtos_rpmsg_config.h"
#include "rtos_rpmsg_utils.h"

/*
 * This is RPMSG channel info:
 * remote processor, endpoint and name
 * the endpoint exist in Linux rpmsg sysfs:
 * /sys/bus/rpmsg/devices/soc:ipcc@[0|1].[EPT-NAME].-1.[EPT]
 * name should be less than 16 bytes
 */
//#define REMOTE_PROCESSOR             (DP_CA_AP1)
#define LOOP_TEST_FLAG               (1)
#define REMOTE_PROCESSOR             (DP_CA_AP2)

#ifndef SRV_SAMPLE_EPT_NAME
#define SRV_SAMPLE_EPT_NAME          "myendpoint2"
#endif

struct ipcc_channel *globalChan;


static int rtos_rpmsg_recv_thread(void )
{
    int ret = -1;
    unsigned long src = 0;
    char recvMsg[500] = {0};
    int recvLen = 0;
    while(1)
    {
        rtos_rpmsg_recv(globalChan, &src, recvMsg, &recvLen);
        printf("recv qnx msg: %s;len : %d, src: %d\n", recvMsg, recvLen, src);
    }
    
    ipcc_channel_stop(globalChan);
    ipcc_channel_destroy(globalChan);
    return ret;
}

static void rtos_rpmsg_recv_thread_start(void)
{
    thread_t *msgthread;
    printf("creating thread....\n");
    msgthread = thread_create("rpmsg_recv", (thread_start_routine)rtos_rpmsg_recv_thread, NULL,
                            THREAD_PRI_SAMPLE, DEFAULT_STACK_SIZE);
    thread_detach_and_resume(msgthread);
}

#if LOOP_TEST_FLAG
    static void rpmsg_intf_entry(int argc, const cmd_args *argv)
    {
        printf("rpmsg_intf_entry-->\n");

        struct _loopTest_payload localPayload = {0};

        char recvMsg[492] = {};
        char sendMsg[492] = {};
        unsigned long src = RPMSG_SAMPLE_QNX_EPT;
        int recvLen = 0;

        globalChan = rtos_rpmsg_init();
        printf("---mtu:%d\n", globalChan->mtu);
        thread_sleep(10000);   //1000ms

        while (1)
        {
            rtos_rpmsg_recv(globalChan, &src, recvMsg, &recvLen);
            printf("recvLen:%d. src:%d.\n", recvLen, src);
            
            memcpy(&localPayload, recvMsg, sizeof(localPayload));
            localPayload.recvIndex = localPayload.index+1;
            printf("--->index:%d, recvIndex:%d.\n", localPayload.index, localPayload.recvIndex);
            memcpy(sendMsg, &localPayload, sizeof(localPayload));
            // printf("xyzsend msg..%d..\n", sizeof(localPayload));
            // rtos_rpmsg_send(globalChan, src, sendMsg, PAYLOAD_MAX_SIZE);
            rtos_rpmsg_send(globalChan, src, sendMsg, sizeof(localPayload));
            // thread_sleep(1000);   //1000ms
            
        }
        

    }
#else
    static void rpmsg_intf_entry(int argc, const cmd_args *argv)
    {
        printf("rpmsg_intf_entry-->\n");
        char sendMsg[484] = "msg rtos->qnx: hello world!hello world!hello world!hello world!hello world!hello world!hello world!";
        unsigned long dst = 0;
        
        globalChan = rtos_rpmsg_init();
        
        rtos_rpmsg_recv_thread_start();
        
        dst = RPMSG_SAMPLE_QNX_EPT;

        while (1)
        {
            printf("send msg..%d..\n", strlen(sendMsg));
            // rtos_rpmsg_send(globalChan, dst, sendMsg, FIXEDLEN_MSG_PAYLOAD_SIZE);
            rtos_rpmsg_send(globalChan, dst, sendMsg, strlen(sendMsg));
    	    // ipcc_channel_sendto(globalChan, src, sendMsg, 492, WAIT_RPMSG_DEV_TIMEOUT);
            thread_sleep(1000);   //1000ms
        }
        

    }
#endif

#if defined(WITH_LIB_CONSOLE)
STATIC_COMMAND_START STATIC_COMMAND("rpmsg_intf", "run a sample service",
                                    (console_cmd)&rpmsg_intf_entry)
STATIC_COMMAND_END(rpmsg_intf);
#endif

