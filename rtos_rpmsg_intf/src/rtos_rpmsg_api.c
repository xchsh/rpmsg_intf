#include "rtos_rpmsg_api.h"

static int rpmsg_fixedLen_package(struct _rpmsg_fixedLen_message *fixedLenMsg, uint8_t *sendMsg, uint16_t len);
static int rpmsg_fixedLen_unpackage(struct _rpmsg_fixedLen_message *fixedLenMsg, uint8_t *recvMmsg, uint16_t len);

struct ipcc_channel* rtos_rpmsg_init(void)
{
    int ret = 0;
    
    struct ipcc_device *intfDev;
    struct ipcc_channel *localChan = malloc(sizeof(struct ipcc_channel));
    
    intfDev = ipcc_device_gethandle(IPCC_RRPOC_AP2, DEFAULT_TIMEOUT);
	if (intfDev == NULL) {
		printf("ipcc_device_gethandle error\n");
		// return ret;
	}
	localChan = ipcc_channel_create(intfDev, RPMSG_SAMPLE_RTOS_EPT, QNX_MSG_IPCC_DESC, true);
	if (localChan == NULL) {
		printf("ipcc_channel_create error\n");
		// return ret;
	}
    localChan->mtu = IPCC_MB_MTU;
	ret = ipcc_channel_start(localChan, NULL);
	if (ret < 0) {
		printf("ipcc_channel_start error:%d\n", ret);
		// return ret;
	}
	printf("!qnx_msg_service_init\n");
    return localChan;
}

int rtos_rpmsg_send(struct ipcc_channel *locChan, unsigned long dst, char *t_payload, int t_payload_len)
{
    int ret = -1;
    struct _rpmsg_fixedLen_message tmpMsg = {0};
    unsigned char sendMsg[MAX_RPMSG_BUFF_SIZE] = {0};

    if((t_payload_len<=0)||(t_payload_len>FIXEDLEN_MSG_PAYLOAD_SIZE))
    {
        ret = -1;
        printf("write intf len error, t_payload_len:%d.\n", t_payload_len);
    }
    else
    {
        if(dst == RPMSG_SAMPLE_QNX_EPT)
        {
            rpmsg_fixedLen_package(&tmpMsg, (uint8_t*)t_payload, t_payload_len);
            ret = ipcc_channel_sendto(locChan, dst, (char*)&tmpMsg, sizeof(tmpMsg), INFINITE_TIME);
        }
        else
        {
            ret = -1;
            printf("dst error, not init value. dst:%d.\n", dst);
        }
        
    }
    

    return ret;
}

int rtos_rpmsg_recv(struct ipcc_channel *locChan, unsigned long *src, char *t_payload, int *msgLen)
{
    int ret = -1;
    struct _rpmsg_fixedLen_message tmpMsg = {0};
    int crcValue = 0;


    int t_payload_len = RPMSG_MTU;

    ret = ipcc_channel_recvfrom(locChan, src, (char*)&tmpMsg, &t_payload_len, INFINITE_TIME);
    if (t_payload_len == MAX_RPMSG_BUFF_SIZE)
    {
        crcValue = crc16_checksum(tmpMsg.len, (uint8_t*)&(tmpMsg.msg));
        if (crcValue == tmpMsg.checksum)
        {
            memcpy(t_payload, &(tmpMsg.msg), tmpMsg.len);
            *msgLen = tmpMsg.len;
            printf("carValue:%d, tmpMsg.checksum:%d. src:%d, tmpMsg.srcId:%d. \n", crcValue, tmpMsg.checksum, *src, tmpMsg.srcId);
        }
        else
        {
			printf("crcValue err.\n");
            ret = -1;
        }
    }
    return ret;
}


static int rpmsg_fixedLen_package(struct _rpmsg_fixedLen_message *fixedLenMsg, uint8_t *sendMsg, uint16_t len)
{
    int ret = -1;
    uint16_t crcValue = 0;
    fixedLenMsg->len = len;
    fixedLenMsg->srcId = RPMSG_SAMPLE_QNX_EPT;
    fixedLenMsg->dstId = RPMSG_SAMPLE_RTOS_EPT;

    memcpy(fixedLenMsg->msg, sendMsg, len);
    /* calculate payload crc value */
    crcValue = crc16_checksum(len, sendMsg);

    fixedLenMsg->checksum = crcValue;


    return ret;
}

static int rpmsg_fixedLen_unpackage(struct _rpmsg_fixedLen_message *fixedLenMsg, uint8_t *recvMmsg, uint16_t len)
{
    int ret = -1;
    

    return ret;
}

