#ifndef _SSP_API_H_
#define _SSP_API_H_


#include <stdlib.h>
#include <assert.h>
#include <bits.h>
#include <debug.h>
#include <stdio.h>
#include <err.h>
#include <kernel/thread.h>
#include <kernel/event.h>
#include <lk/init.h>
#include <lib/console.h>
#include <lib/bytes.h>
#include <lib/reg.h>
#include <app.h>

/* must include this for ipcc interface */
#include <dcf.h>

/* must include this for thread priority
 * set thread proirity is system policy
 */
#include <sys_priority.h>
#include "rtos_rpmsg_utils.h"


#define RPMSG_SAMPLE_RTOS_EPT  (550)
#define RPMSG_SAMPLE_QNX_EPT   (551)

#define QNX_MSG_IPCC_DESC "qnx_msg"
#define DEFAULT_TIMEOUT (1000 * 5)


/* HERE IS RPMSG MTU (may 492 Bytes), selef head 8 Bytes */
#define RPMSG_MTU                   (508)
#define PAYLOAD_MIN_SIZE            (1)
#define PAYLOAD_MAX_SIZE            (508 - 16 - 8)

/* app msg struct */
#define APPDATASIZE            (473)
struct _loopTest_payload {
    uint32_t index;
    uint32_t recvIndex;
    uint8_t size;
    uint8_t arg1;
    uint8_t arg2;
    char data[APPDATASIZE];
}__attribute__((__packed__));   /*  struct size must <= 484(FIXEDLEN_MSG_PAYLOAD_SIZE), 11+473 */


/* Fixed length message format, app msg maxLen: 508-16-8 = 484 Bytes */
#define RPMSG_HEADER_LEN        (16)
#define MAX_RPMSG_BUFF_SIZE     (508 - RPMSG_HEADER_LEN)
#define FIXEDLEN_MSG_PAYLOAD_SIZE    (MAX_RPMSG_BUFF_SIZE - 8)
struct _rpmsg_fixedLen_message
{
    uint16_t    len;
    uint16_t    srcId;
    uint16_t    dstId;
    uint8_t     msg[FIXEDLEN_MSG_PAYLOAD_SIZE];
    uint16_t    checksum;
}__attribute__((__packed__));


struct ipcc_channel* rtos_rpmsg_init(void);
int rtos_rpmsg_send(struct ipcc_channel *locChan, unsigned long dst, char *t_payload, int t_payload_len);   /* dst: peer endpoint vaule */
int rtos_rpmsg_recv(struct ipcc_channel *locChan, unsigned long *src, char *t_payload, int *msgLen);  /* src: peer endpoint vaule */

#endif //_SSP_API_H_
