
#include "main.h"
#include "inc.h"
#include <string.h>


#define MY_MSGID    (503)
#define MY_EVENTID  (3)
#define MY_INDEX    (3)
#define MY_APPID    (403)
#define MY_TASK_NAME    "hostTask"

#define PEER_APPID  (303)
#define PEER_APP_NAME   "QNX.sdcast.app3"

/**************************** 任务句柄 ********************************/
/*
* 任务句柄是一个指针，用于指向一个任务，当任务创建好之后，它就具有了一个任务句柄
* 以后我们要想操作这个任务都需要通过这个任务句柄，如果是自身的任务操作自己，那么
* 这个句柄可以为NULL。
*/
static TaskHandle_t AppTaskCreate_Handle = NULL;/* 创建任务句柄 */
static TaskHandle_t Receive_Task_Handle = NULL;/* LED 任务句柄 */
static TaskHandle_t Send_Task_Handle = NULL;/* KEY 任务句柄 */

/*
*************************************************************************
* 函数声明
*************************************************************************
*/
static void AppTaskCreate(void);/* 用于创建任务 */
static void Receive_Task(void* pvParameters);/* Receive_Task 任务实现 */
static void Send_Task(void* pvParameters);/* Send_Task 任务实现 */


/*****************************************************************
* @brief 主函数
* @param 无
* @retval 无
* @note 第一步：开发板硬件初始化
第二步：创建APP 应用任务
第三步：启动FreeRTOS，开始多任务调度
****************************************************************/
// int task_comm_test1(int argc, const char *argv)
static void task_comm_test1(const struct app_descriptor *app, void *args)
{
    BaseType_t xReturn = pdPASS;/* 定义一个创建信息返回值，默认为pdPASS */

    printf("[task1]- this is task1 recv proxy message.\n");

    /* params init ensure. */
    int i=0;
    for (i = 0; i < MAX_TASK_NUM; ++i)
    {
        /* code */
        if ((MY_MSGID==commTopoInfo[i].msgId)&&(MY_EVENTID==commTopoInfo[i].eventId))
        {
            printf("[task1]- this task1 belong to proxy define. taskName is %s.\n", commTopoInfo[i].hostTaskName);
        }
    }


    
    /* 创建AppTaskCreate 任务 */
    xReturn = xTaskCreate((TaskFunction_t )AppTaskCreate,           /* 任务入口函数 */
                                (const char* )"AppTaskCreate",      /* 任务名字 */
                                (uint16_t )512,                     /* 任务栈大小 */
                                (void* )NULL,                       /* 任务入口函数参数 */
                                (UBaseType_t )1,                    /* 任务的优先级 */
                                (TaskHandle_t* )&AppTaskCreate_Handle);/* 任务控制块指*/
    
    printf("[task1]-xReturn is %d.\n", xReturn);
    

    while (1)
    {
        vTaskDelay(2000);
    }
}


/***********************************************************************
* @ 函数名 ： AppTaskCreate
* @ 功能说明： 为了方便管理，所有的任务创建函数都放在这个函数里面
* @ 参数 ： 无
* @ 返回值 ： 无
********************************************************************/
static void AppTaskCreate(void)
{
    BaseType_t xReturn = pdPASS;/* 定义一个创建信息返回值，默认为pdPASS */

    taskENTER_CRITICAL(); //进入临界区

    xReturn = xTaskCreate((TaskFunction_t )Send_Task,               /* 任务入口函数 */
                            (const char* )"Send_Task",              /* 任务名字 */
                            (uint16_t )1024,                         /* 任务栈大小 */
                            (void* )NULL,                           /* 任务入口函数参数 */
                            (UBaseType_t )3,                        /* 任务的优先级 */
                            (TaskHandle_t* )&Send_Task_Handle);     /*任务控制块指针 */
    if (pdPASS == xReturn)
        printf("[task1]-创建Send_Task 任务成功!\n\n");


    
    vTaskDelay(20);

    /* 创建Receive_Task 任务 */
    xReturn = xTaskCreate((TaskFunction_t )Receive_Task,            /* 任务入口函数 */
                            (const char* )"Receive_Task",           /* 任务名字 */
                            (uint16_t )1024,                         /* 任务栈大小 */
                            (void* )NULL,                           /* 任务入口函数参数 */
                            (UBaseType_t )2,                        /* 任务的优先级 */
                            (TaskHandle_t* )&Receive_Task_Handle);  /*任务控制块指针*/
    if (pdPASS == xReturn)
        printf("[task1]-创建Receive_Task 任务成功!\r\n");

    vTaskDelete(AppTaskCreate_Handle); //删除AppTaskCreate 任务

    taskEXIT_CRITICAL(); //退出临界区
}



static void Receive_Task(void* parameter)
{
    BaseType_t xReturn = pdTRUE;/* 定义一个创建信息返回值，默认为pdTRUE */
    uint32_t r_queue; /* 定义一个接收消息的变量 */
    int i = 0;
    char recvStr[492] = {};
    struct _dispatch_msg_value localDispatchInfo = {};
    EventBits_t r_event; /* 定义一个事件接收变量 */
    EventBits_t eventId = MY_EVENTID;
    // char sendStr[100] = "hello, i am the task.";
    printf("[task1]-this is receive task.\n");
    while (1) 
    {
        r_event = xEventGroupWaitBits(Event_proxy2task, /* 事件对象句柄 */
                                        eventId,/* 接收任务感兴趣的事件 */
                                        pdTRUE, /* 退出时清除事件位 */
                                        pdTRUE, /* 满足感兴趣的所有事件 */
                                        portMAX_DELAY);/* 指定超时事件,一直等 */
        if (r_event && eventId)
        {
            /* code */
            printf("[task1]-received event notify! eventId:%d. \n", eventId);
            
            if (pdTRUE == xReturn)
            {
                memcpy(&localDispatchInfo, &DispatchInfo[MY_INDEX], sizeof(struct _dispatch_msg_value));
                printf("[task1]-msgId:%d, eventId:%d, msg:%s\n\n",localDispatchInfo.msgId, localDispatchInfo.eventId, localDispatchInfo.msg);   
            }
            else
            {
                printf("[task1]-数据接收出错,错误代码: 0x%lx\n",xReturn);
            }
        }
        else
        {
            printf ( "事件错误！\n");
        }

    }
}


static void Send_Task(void* parameter)
{
    BaseType_t xReturn = pdPASS;/* 定义一个创建信息返回值，默认为pdPASS */
    printf("[task1]-xxxxxxxxxyyyyyyyyzzzzzzz！\n");
    char sendStr1[492] = {};
    char sendMsg[] = "hello, i am task1.";
    struct _dispatch_msg_value localDispatchInfo = {};
    localDispatchInfo.index = MY_INDEX;
    localDispatchInfo.msgId = MY_MSGID;

    localDispatchInfo.peerAppId = PEER_APPID;
    // memcpy(&localDispatchInfo.peerTaskName, PEER_APP_NAME, sizeof(PEER_APP_NAME));
    
    memcpy(localDispatchInfo.msg, sendMsg, sizeof(sendMsg));
    memcpy(sendStr1, &localDispatchInfo, sizeof(struct _dispatch_msg_value));

    while (1) 
    {
        
        xReturn = xQueueSend( CommQueue_task2proxy, /* 消息队列的句柄 */
                                    sendStr1,       /* 发送的消息内容 */
                                    0 );            /* 等待时间 0 */
        if (pdPASS == xReturn)
        {
            printf("[task1]-消息send_data1 发送成功!\n\n");
        }
        
        vTaskDelay(2000);/* 延时20 个tick */
    }
}


/*******************************END OF FILE****************************/

#if defined(WITH_LIB_CONSOLE)
STATIC_COMMAND_START STATIC_COMMAND("task_comm_test1", "run a sample service",
                                    (console_cmd)&task_comm_test1)
STATIC_COMMAND_END(task_comm_test1);
#endif

/*
APP_START(task_comm_test1)
.flags = 0,
.entry = task_comm_test1,
APP_END
*/