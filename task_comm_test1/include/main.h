#ifndef _MAIN_H_
#define _MAIN_H_
#include <FreeRTOS.h>
#include <task.h>
#include <event_groups.h>
#include <stdlib.h>
#include <assert.h>
#include <bits.h>
#include <debug.h>
#include <stdio.h>
#include <err.h>
#include <kernel/thread.h>
#include <kernel/event.h>
#include <lk/init.h>
#include <lib/console.h>
#include <lib/bytes.h>
#include <lib/reg.h>
#include <app.h>


//#define XCHSH_QUEUE_LEN 4 /* 队列的长度，最大可包含多少个消息 */
//#define XCHSH_QUEUE_SIZE 4 /* 队列中每个消息大小（字节） */
// 
#endif