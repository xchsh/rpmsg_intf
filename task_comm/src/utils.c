#include "utils.h"
#include "inc.h"
/*ini_handle.c*/
#include <stdio.h>  
#include <stdlib.h>
#include <string.h>

#define R5_TASK_NUM			(5)

struct _comm_topology_config commTopoInfoCfg[R5_TASK_NUM] = {
	{0, 500, 0,  400, "R5.module.Task0", 300, "QNX.module.app0"},
	{1, 501, 1,  401, "R5.module.Task1", 301, "QNX.module.app1"},
	{2, 502, 2,  402, "R5.module.Task2", 302, "QNX.module.app2"},
	{3, 503, 3,  403, "R5.module.Task3", 303, "QNX.module.app3"}
};



int utils_cfg_init(void)
{
	int ret = -1;
	int i = 0;
	if (R5_TASK_NUM < MAX_TASK_NUM)
	{
		#if 0
		/* cfg check*/
		for ( i = 0; i < R5_TASK_NUM; ++i)
		{
			if((i==commTopoInfoCfg[i].index)&&(i==commTopoInfoCfg[i].eventId))
			{
				ret = 0;
			}
			else
			{
				ret = -1;
				printf("cfg check error!\n");
				return ret;
			}
		}
		#endif

		for ( i = 0; i < R5_TASK_NUM; ++i)
		{
			commTopoInfoCfg[i].eventId = -1;
		}

		for ( i = 0; i < R5_TASK_NUM; ++i)
		{
			memcpy(&commTopoInfo[i], &commTopoInfoCfg[i], sizeof(struct _comm_topology_config));
		}

		for ( i = 0; i < R5_TASK_NUM; ++i)
		{
			DispatchInfo[i].index = i;
			DispatchInfo[i].appId = commTopoInfo[i].hostAppId;
			DispatchInfo[i].eventId = commTopoInfo[i].eventId;
			DispatchInfo[i].msgId = commTopoInfo[i].eventId;
			memcpy(DispatchInfo[i].msg, 0, sizeof(DispatchInfo[i].msg));
		}
	}

	return ret;
}