#include <string.h>
#include "proxy_handle.h"
#include "inc.h"
#include "utils.h"


EventGroupHandle_t Event_proxy2task = NULL;
EventGroupHandle_t Event_task2proxy = NULL;

QueueHandle_t CommQueue_proxy2task = NULL;
QueueHandle_t CommQueue_task2proxy = NULL;

static TaskHandle_t AppTaskCreate_Handle = NULL;	/* 创建任务句柄 */
static TaskHandle_t send_proxyMsg_Handle = NULL;
static TaskHandle_t recv_proxyMsg_Handle = NULL;
static TaskHandle_t dispatch_update_Handle = NULL;

struct _rpmsg_fixedLen_message commMsg[5] = {};
struct _dispatch_msg_value tmpDispatchInfo = {};
struct _dispatch_msg_value DispatchInfo[MAX_TASK_NUM] = {};
struct _comm_topology_config commTopoInfo[MAX_TASK_NUM] = {};
// struct _comm_topology_config commTopoInfoCfg[] = {COMM_TOPO_CFG_DEFINE};

/* local functions */
static int queue_and_event_init(void);
static int proxy_handle_task_start(void);
static void recv_proxyMsg_Task(void* parameter);
static void send_proxyMsg_Task(void* parameter);
static void dispatch_updateValue_task(void* parameter);
static int analog_data_init(void);
static int comm_topo_info_init(void);


int task_comm_proxy_handle_init(void)
{
	int ret = -1;

	ret = queue_and_event_init();

	ret = proxy_handle_task_start();

	return ret;
}


static int analog_data_init(void)
{
    int i = 0;
    int ret = 0;
    char str[] = "hello hahah haha 0123456789";

    for (i = 0; i < 5; i++)
    {
        commMsg[i].len = 40;
        commMsg[i].srcId = 500 + i;
        commMsg[i].dstId = 400 + i;
        commMsg[i].eventId = i;
        commMsg[i].index = i;
        commMsg[i].msgId = 500+i;
        commMsg[i].appId = 400+i;
        memcpy(commMsg[i].msg, str, strlen(str)-i);
        commMsg[i].checksum = 251;

    }
    return ret;
} 



static int comm_topo_info_init(void)
{
    // char hostTaskName[] = "hostTask";
    // char peerTaskName[] = "peerTask";

    // int i = 0;
    // for ( i = 0; i < MAX_TASK_NUM; ++i)
    // {
    //     commTopoInfo[i].msgId = 500+i;
    //     commTopoInfo[i].eventId = i;
    //     memcpy(commTopoInfo[i].hostTaskName, hostTaskName, sizeof(hostTaskName));
    //     commTopoInfo[i].hostAppId = 400+i;
    //     commTopoInfo[i].peerAppId = 300+i;
    //     memcpy(commTopoInfo[i].peerTaskName, peerTaskName, sizeof(peerTaskName));

    //     /* need or needn't */
    //     DispatchInfo[i].index = i;
    //     DispatchInfo[i].appId = 400+i;
    //     DispatchInfo[i].eventId = i;
    //     DispatchInfo[i].msgId = 500+i;
    //     memcpy(DispatchInfo[i].msg, 0, sizeof(DispatchInfo[i].msg));
    // }

    return utils_cfg_init();
}



static int queue_and_event_init(void)
{
	int ret = -1;
	int i = 0;

    analog_data_init();
    comm_topo_info_init();

    Event_proxy2task = xEventGroupCreate();
    if (NULL != Event_proxy2task)
    {
    	printf("[comm_proxy]- Event_proxy2task 事件创建成功!\r\n");
    }

    Event_task2proxy = xEventGroupCreate();
    if (NULL != Event_task2proxy)
    {
    	printf("[comm_proxy]- Event_task2proxy 事件创建成功!\r\n");
    }
    
    /* CommQueue_proxy2task CommQueue_task2proxy */
    CommQueue_proxy2task = xQueueCreate((UBaseType_t ) QUEUE_LEN,           /* 消息队列的长度 */
                                		(UBaseType_t ) QUEUE_SIZE);           /* 消息的大小 */
    if (NULL != CommQueue_proxy2task)
    {
        printf("[comm_proxy]- 创建 CommQueue_proxy2task 消息队列成功!\r\n");
    }
    CommQueue_task2proxy = xQueueCreate((UBaseType_t ) QUEUE_LEN,        /* 消息队列的长度 */
                                		(UBaseType_t ) QUEUE_SIZE);        /* 消息的大小 */
    if (NULL != CommQueue_task2proxy)
    {
        printf("[comm_proxy]- 创建 CommQueue_task2proxy 消息队列成功!\r\n");
    }

	return ret;
}


static int proxy_handle_task_start(void)
{
	int ret = -1;
	BaseType_t xReturn = pdPASS;

    taskENTER_CRITICAL();
    
    xReturn = xTaskCreate((TaskFunction_t )recv_proxyMsg_Task, /* 任务入口函数 */
                            (const char* )"recv_proxyMsg_Task",/* 任务名字 */
                            (uint16_t )1024, /* 任务栈大小 */
                            (void* )NULL, /* 任务入口函数参数 */
                            (UBaseType_t )2, /* 任务的优先级 */
                            (TaskHandle_t* )&recv_proxyMsg_Handle);/* 任务控制块指针 */
    if (pdPASS == xReturn)
    {
    	printf("[comm_proxy]- 创建recv_proxyMsg_Task 任务成功!\r\n");
    }

    
    xReturn = xTaskCreate((TaskFunction_t )send_proxyMsg_Task, /* 任务入口函数 */
                            (const char* )"send_proxyMsg_Task",/* 任务名字 */
                            (uint16_t )1024, /* 任务栈大小 */
                            (void* )NULL,/* 任务入口函数参数 */
                            (UBaseType_t )3, /* 任务的优先级 */
                            (TaskHandle_t* )&send_proxyMsg_Handle);/* 任务控制块指针 */
    if (pdPASS == xReturn)
        printf("[comm_proxy]- 创建send_proxyMsg_Task 任务成功!\n");

    
    xReturn = xTaskCreate((TaskFunction_t )dispatch_updateValue_task, /* 任务入口函数 */
                            (const char* )"dispatch_updateValue_task",/* 任务名字 */
                            (uint16_t )1024, /* 任务栈大小 */
                            (void* )NULL,/* 任务入口函数参数 */
                            (UBaseType_t )3, /* 任务的优先级 */
                            (TaskHandle_t* )&dispatch_update_Handle);/* 任务控制块指针 */
    if (pdPASS == xReturn)
        printf("[comm_proxy]- 创建dispatch_updateValue_task 任务成功!\n");


    taskEXIT_CRITICAL(); //退出临界区

	return ret;
}


/* msg: ipcc -> proxy -> tasks */
static void recv_proxyMsg_Task(void* parameter)
{
	BaseType_t xReturn = pdTRUE;
	char recvMsg[492] = "hello, i am proxy.";
    int i = 0;
	while(1)
	{
		/* 1. task_comm_recvfrom_Proxy get message */
        tmpDispatchInfo.index = i;
        tmpDispatchInfo.msgId = commMsg[i].msgId;
        tmpDispatchInfo.appId = commMsg[i].appId;
        tmpDispatchInfo.eventId = commMsg[i].eventId;
        memcpy(&tmpDispatchInfo.msg, &commMsg[i].msg, sizeof(commMsg[i].msg));

        memcpy(recvMsg, &tmpDispatchInfo, sizeof(struct _dispatch_msg_value));

		/* 2. put the message to queue */
		xReturn = xQueueSend( CommQueue_proxy2task, 						/* 消息队列的句柄 */
								&recvMsg,									/* 发送的消息内容 */
								0 ); 										/* 等待时间 0 */
		if (pdPASS == xReturn)
		{
            i++;
            if(i == 4)
            {
                i = 0;
            }
		}
        else
        {
            printf("xQueueSend error.\n");
        }
		vTaskDelay(2000);
	}
}

/* msg: tasks -> proxy -> ipcc */
static void send_proxyMsg_Task(void* parameter)
{   
    /* recv msg from taskQueue */
    BaseType_t xReturn = pdTRUE;
    char recvMsg[492] = {};
    struct _dispatch_msg_value localDispatchInfo = {};
    while(1)
    {
        xReturn = xQueueReceive( CommQueue_task2proxy,    /* 消息队列的句柄 */
                                    recvMsg,              /* 发送的消息内容 */
                                    portMAX_DELAY );      /* 等待时间 一直等 */
        if (pdPASS == xReturn)
        {
            memcpy(&localDispatchInfo, recvMsg, sizeof(localDispatchInfo));
            printf("[comm_proxy]- peerAppId:%d, recvMsg: %s\n\n",localDispatchInfo.peerAppId, localDispatchInfo.msg);
        }
    }
}

/* get msg from proxyQueue, and dispacth update globalValue, then set Event */
static void dispatch_updateValue_task(void* parameter)
{
    BaseType_t xReturn = pdTRUE;
    char recvStr[492] = {};
    int i = 0;

    while(1)
    {
        xReturn = xQueueReceive( CommQueue_proxy2task,      /* 消息队列的句柄 */
                                    recvStr,                /* 发送的消息内容 */
                                    portMAX_DELAY);         /* 等待时间 一直等 */
        if (pdTRUE == xReturn)
        {
            /* 1. 解析 */
            memcpy(&tmpDispatchInfo, recvStr, sizeof(tmpDispatchInfo));
            printf("[comm_proxy]-%s-, index:%d.\n", tmpDispatchInfo.msg, tmpDispatchInfo.index);
            
            /* 2. 更新对应的全局变量 */
            // for (i = 0; i < MAX_TASK_NUM; ++i)
            // {
            //     if(tmpDispatchInfo.eventId == DispatchInfo[i].eventId)
            //     {
            //         memcpy(&DispatchInfo[i], &tmpDispatchInfo, sizeof(tmpDispatchInfo));
                    
            //         xEventGroupSetBits(Event_proxy2task, tmpDispatchInfo.eventId);
            //         printf("[comm_proxy] - event ok, eventId:%d, i:%d.\n", tmpDispatchInfo.eventId, i);
            //     }
            // }
            memcpy(&DispatchInfo[tmpDispatchInfo.index], &tmpDispatchInfo, sizeof(tmpDispatchInfo));
            xEventGroupSetBits(Event_proxy2task, tmpDispatchInfo.eventId);
        }
        else
        {
            printf("[comm_proxy]-数据接收出错,错误代码: 0x%lx\n",xReturn);
        }

        vTaskDelay(10);
    }
}
