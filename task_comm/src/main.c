#include "inc.h"
#include "proxy_handle.h"

static TaskHandle_t AppTaskCreate_Handle = NULL;/* 创建任务句柄 */

EventGroupHandle_t Event_Handle[MAX_EVENT_HANDLE_NUM] = {NULL};
QueueHandle_t TaskComm_Queue = NULL;


/*
*************************************************************************
* 函数声明
*************************************************************************
*/
static void AppTaskCreate(void);/* 用于创建任务 */


/***********************************************************************
* @ 函数名 ： AppTaskCreate
* @ 功能说明： 为了方便管理，所有的任务创建函数都放在这个函数里面
* @ 参数 ： 无
* @ 返回值 ： 无

******************************************************************/
static void AppTaskCreate(void)
{
    BaseType_t xReturn = pdPASS;/* 定义一个创建信息返回值，默认为pdPASS */
    

    taskENTER_CRITICAL(); //进入临界区

    
    vTaskDelete(AppTaskCreate_Handle); //删除AppTaskCreate 任务

    taskEXIT_CRITICAL(); //退出临界区
}



static void task_comm(const struct app_descriptor *app, void *args)
// int task_comm(int argc, const char *argv)
{
    BaseType_t xReturn = pdPASS;/* 定义一个创建信息返回值，默认为pdPASS */

   
    printf("这是一个 task_comm 代理实验！\n");

    task_comm_proxy_handle_init();

    while (1)
    {
        vTaskDelay(2000);
    }

    printf("hello world.\n");

}


/*
#if defined(WITH_LIB_CONSOLE)
STATIC_COMMAND_START STATIC_COMMAND("task_comm_intf", "run a sample service",
                                    (console_cmd)&task_comm)
STATIC_COMMAND_END(task_comm_intf);
#endif
*/

APP_START(task_comm)
.flags = 0,
.entry = task_comm,
APP_END
