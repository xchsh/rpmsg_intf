#ifndef _INC_H_
#define _INC_H_

/* FreeRTOS 头文件 */
#include <FreeRTOS.h>
#include <task.h>
#include <event_groups.h>
#include <queue.h>
#include <stdlib.h>
#include <assert.h>
#include <bits.h>
#include <debug.h>
#include <stdio.h>
#include <err.h>
#include <kernel/thread.h>
#include <kernel/event.h>
#include <lk/init.h>
#include <lib/console.h>
#include <lib/bytes.h>
#include <lib/reg.h>
#include <app.h>

#define QUEUE_LEN                   10  /* 队列的长度，最大可包含多少个消息 */
#define QUEUE_SIZE                  492 /* 队列中每个消息大小（字节）,proxy struct */

#define MAX_TASK_NUM				(10)

#define MAX_EVENT_HANDLE_NUM        (MAX_TASK_NUM)

extern EventGroupHandle_t Event_proxy2task;
extern EventGroupHandle_t Event_task2proxy;

extern QueueHandle_t CommQueue_proxy2task;
extern QueueHandle_t CommQueue_task2proxy;

/* global config info */
struct _dispatch_msg_value
{
	int index;
	int appId;
	EventBits_t eventId;
	int msgId;
	int peerAppId;
	char msg[40];
}__attribute__((__packed__));

extern struct _dispatch_msg_value DispatchInfo[MAX_TASK_NUM];


struct _comm_topology_config
{
	int index;
	int msgId;
	EventBits_t eventId;
	int hostAppId;
	char hostTaskName[20];
	int peerAppId;
	char peerTaskName[20];
};

extern struct _comm_topology_config commTopoInfo[MAX_TASK_NUM];

#endif