#ifndef _UTILS_H_
#define _UTILS_H_
#include "inc.h"

#define YF_LOG_SUCCESS		(0)
#define YF_LOG_ERROR		(-1)

#define TASK_COMM_CFG_PATH		("../../task_comm.ini")

int utils_cfg_init(void);


#endif