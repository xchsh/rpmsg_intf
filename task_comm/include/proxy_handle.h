#ifndef _PROXY_HANDLE_H_
#define _PROXY_HANDLE_H_
#include <FreeRTOS.h>
#include <event_groups.h>


struct _rpmsg_fixedLen_message
{
    int    		len;		/* "msg[FIXEDLEN_MSG_PAYLOAD]实际报文长度 " */
    int    		srcId;
    int    		dstId;
    EventBits_t eventId;
    int    		index;
    int 		msgId;
    int 		appId;
    char   		msg[40];
    int    		checksum;
}__attribute__((__packed__));


int task_comm_proxy_handle_init(void);

#endif