#ifndef DEFINE_H
#define DEFINE_H

typedef unsigned char                    UINT8;
typedef unsigned short int               UINT16;
typedef unsigned int                     UINT32;
typedef unsigned long int                UINT64;

#endif