#ifndef __RPMSG_SAMPLE_INTF_H
#define __RPMSG_SAMPLE_INTF_H
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "rpmsg/rpmsg.h"


#if 1
#define RUN_ON_INTF
#else
#define RUN_ON_CMD
#endif


/* The sample compatiable version with Linux */
#define SAMPLE_TEST_VERSION "v211015"
/* The default remote device: safety */
#define RPMSG_DEVICE_NAME   "/dev/rpmsg10"      /*IPCC safety*/
//#define RPMSG_DEVICE_NAME   "/dev/rpmsg3"      /*IPCC AP1 yocto*/
/* User-defined endpoint in RTOS and QNX */
#define RPMSG_SAMPLE_RTOS_EPT  (550)
#define RPMSG_SAMPLE_QNX_EPT   (551)



#define RPMSG_HEADER_LEN        (16)
#define MAX_RPMSG_BUFF_SIZE     (508 - RPMSG_HEADER_LEN)
#define SAMPLE_SEND_TIMES       (10)
#define FIXEDLEN_MSG_PAYLOAD    (MAX_RPMSG_BUFF_SIZE - 8)


/* Fixed length message format */
struct _rpmsg_fixedLen_message
{
    uint16_t    len;
    uint16_t    srcId;
    uint16_t    dstId;
    uint8_t     msg[FIXEDLEN_MSG_PAYLOAD];
    uint16_t    checksum;
}__attribute__((__packed__));



int rpmsg_ipcc_fd_init();
int rpmsg_ipcc_write_intf(int fd, unsigned char *buf, uint16_t msgLen, bool complete); /* complete==true */
int rpmsg_ipcc_select_read_intf(int fd, unsigned char *buf, uint16_t len, uint16_t *msgLen, bool complete); /* complete==true */
#endif