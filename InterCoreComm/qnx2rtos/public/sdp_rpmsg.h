
#ifndef SDP_RPMSG_H
#define SDP_RPMSG_H
/*  includes */
#include "define.h"

/* global definitions */
#define MAX_SDP_PRE_READ_TCP_SIZE (1000)

/* global typedefs */
typedef struct _SdpPreReadTcpMsg
{
    int comId;
    int msgType;
    int msgLen;
    char byteArray[492];
}SdpPreReadTcpMsg;

typedef struct _SdpPreReadTcpBuf
{
    SdpPreReadTcpMsg    msgArray[MAX_SDP_PRE_READ_TCP_SIZE];   /*message frame*/
    UINT16  front;  /*front pointer*/
    UINT16  rear;   /*rear pointer*/
}SdpPreReadTcpBuf;

/* global variables*/

/* global functions declarations */
extern int sdp_buf_mq_pre_read_tcp_init(SdpPreReadTcpBuf *pBuf);
extern int sdp_buf_mq_pre_read_tcp_put_data(SdpPreReadTcpBuf *pBuf, SdpPreReadTcpMsg *pMsg);
extern int sdp_buf_mq_pre_read_tcp_get_data(SdpPreReadTcpBuf *pBuf, SdpPreReadTcpMsg *pMsg);
#endif /* SDP_TCP_H */


