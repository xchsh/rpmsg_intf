#ifndef _UTILS_H__
#define _UTILS_H__
#include <stdint.h>

uint16_t crc16_checksum(uint16_t len, const uint8_t *data);

#endif