/*
 * $QNXLicenseC:
 * Copyright 2021, QNX Software Systems. All Rights Reserved.
 *
 * You must obtain a written license from and pay applicable license fees to QNX
 * Software Systems before you may reproduce, modify or distribute this software,
 * or any work that includes all or part of this software.   Free development
 * licenses are available for evaluation and non-commercial purposes.  For more
 * information visit http://licensing.qnx.com or email licensing@qnx.com.
 *
 * This file may contain contributions from others.  Please review this entire
 * file for other proprietary rights or license notices, as well as the QNX
 * Development Suite License Guide at http://licensing.qnx.com/license-guide/
 * for other information.
 * $
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/procmgr.h>
#include <sys/neutrino.h>
#include <unistd.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <sys/select.h>
#include <semaphore.h>
#include <pthread.h>
#include "rpmsg/rpmsg.h"
#include "qnx_rpmsg_intf.h"
#include "utils.h"

#define RPMSG_SAMPLE_EVENT_PRIORITY (21)
#define RPMSG_SAMPLE_EVENT_CODE     (SI_NOTIFY)

struct car_mode {
      uint32_t head;  // 0xAA
      uint32_t carMode;
      uint32_t data[8];
};

/* User-defined test command */
enum saf_cmd_type {
    CMD_REPLY_DATA = 0,
    CMD_SEND_DATA,
    CMD_CLOSE_CHANNEL,
    CMD_SEND_DATA_NOB,
};

/* User-defined test case */
enum {
    TEST_TYPE_SEND_ONLY = 1,
    TEST_TYPE_RECV_ONLY,
    TEST_TYPE_CLOSE_REMOTE,
    TEST_TYPE_NONBLOCK,
};

typedef struct {
    uint32_t  n_threads;
    uint32_t  n_data_iters;
    uint32_t  n_seconds;
    char     *rpmsg_node;
    char     *remote_name;
    uint32_t  remote_addr;
    char     *class_name;
    uint32_t  ept_local_addr;
    uint32_t  ept_remote_addr;
    uint32_t  ept_flags;
    uint32_t  test_num;
    int       verbose;
    uint32_t  sleep_ms;
} opts_t;


/*interface name function*/
int rpmsg_ipcc_fd_init();
int rpmsg_ipcc_write_intf(int fd, unsigned char *buf, uint16_t len, bool complete);
int rpmsg_ipcc_select_read_intf(int fd, unsigned char *buf, uint16_t len, uint16_t *msgLen, bool complete);


/* General endpoint function wrapper of open/close/read/write */
static int open_rpmsg_ctl_fd(char *rpmsg_node);
static int rpmsg_char_write(int fd, char *buf, uint32_t len, bool complete);
static int rpmsg_char_select_read(int fd, char *buf, uint32_t len, uint32_t sub_read_len, bool complete);
static int open_rpmsg_ept_fd(int ctl_fd, opts_t *opts);
static int close_rpmsg_ept_fd(int ctl_fd, int ept_fd);
static int rpmsg_fixedLen_package(struct _rpmsg_fixedLen_message *fixedLenMsg, uint8_t *sendMsg, uint16_t len);
static int rpmsg_fixedLen_unpackage(struct _rpmsg_fixedLen_message *fixedLenMsg, uint8_t *recvMmsg, uint16_t len);

static rpmsg_endpoint_t *g_ept_hdl = NULL;
static unsigned ept_id = RPMSG_INVALID_ID;
static int               g_chid;
static int               g_coid;

/* verbose:1, open/close operation, 2: rd/wr data */
static int               verbose;



int rpmsg_ipcc_fd_init()
{
    int ret = 0;
    int opt;
    int ept_fd;

    opts_t opts = {
        .n_threads      = 1,
        .n_data_iters   = 0,
        .rpmsg_node     = RPMSG_DEVICE_NAME,
        .remote_name    = "\0",
        .remote_addr    = RPMSG_NO_ADDR,
        .class_name     = "char",
        .ept_local_addr = RPMSG_SAMPLE_QNX_EPT,
        .ept_remote_addr = RPMSG_SAMPLE_RTOS_EPT,
        .ept_flags      = 0,
        .test_num       = TEST_TYPE_SEND_ONLY,
        .sleep_ms       = 0,
    };
    
    int ctl_fd = open_rpmsg_ctl_fd(opts.rpmsg_node);		/* /dev/rpmsg10 */
    if (ctl_fd < 0) {
        fprintf(stderr, "Failed to open ctl fd! errno=%d(%s)\n\n", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    ept_fd = open_rpmsg_ept_fd(ctl_fd, &opts);
    if (ept_fd < 0) {
        fprintf(stderr, "Failed to get endpoint fd!\n\n");
        close(ctl_fd);
    }
    ret = ept_fd;
    return ret;
}


int rpmsg_ipcc_write_intf(int fd, unsigned char *buf, uint16_t msgLen, bool complete)
{
    int ret = 0;
    struct _rpmsg_fixedLen_message tmpMsg = {0};
    unsigned char sendMsg[MAX_RPMSG_BUFF_SIZE] = {0};
    
    if((msgLen<=0)||(msgLen>FIXEDLEN_MSG_PAYLOAD))
    {
        ret = -1;
        printf("write intf msgLen error, msgLen:%d.\n", msgLen);
    }
    else
    {
        rpmsg_fixedLen_package(&tmpMsg, buf, msgLen);
        // memcpy(sendMsg, tmpMsg, sizeof(tmpMsg));
        ret = rpmsg_char_write(fd, &tmpMsg, sizeof(tmpMsg), complete);
    }
    
    return ret;
}


int rpmsg_ipcc_select_read_intf(int fd, unsigned char *buf, uint16_t len, uint16_t *msgLen, bool complete)
{
    int ret = 0;
    struct _rpmsg_fixedLen_message tmpMsg = {0};
    uint16_t crcValue = 0;
    uint16_t sub_read_len = MAX_RPMSG_BUFF_SIZE;

    if(MAX_RPMSG_BUFF_SIZE == len)
    {
        // ret = rpmsg_char_select_read(fd, buf, len, sub_read_len, complete);
        ret = rpmsg_char_select_read(fd, &tmpMsg, len, sub_read_len, complete);

        crcValue = crc16_checksum(tmpMsg.len, &(tmpMsg.msg));
        if(crcValue == tmpMsg.checksum)
        {
            printf("carValue:%d, tmpMsg.checksum:%d.\n", crcValue, tmpMsg.checksum);
            memcpy(buf, &(tmpMsg.msg), tmpMsg.len);
            *msgLen = tmpMsg.len;
        }
        else
        {
            ret = -1;
            printf("crc checksum error.\n");
        }
    }
    else
    {
        ret = -1;
        printf("select_read_intf len param error, len:%d.\n", len);
    }
    
    return ret;
}


int open_rpmsg_ctl_fd(char *rpmsg_node)
{
    struct sched_param  param;
    pthread_attr_t      attr;

    // Setup the channel and connection for receiving pulse
    if ((g_chid = ChannelCreate(_NTO_CHF_PRIVATE)) < 0) {
        fprintf(stderr, "ChannelCreate failed: errno=%d(%s)\n\n", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    if ((g_coid = ConnectAttach(0, 0, g_chid, _NTO_SIDE_CHANNEL, 0)) < 0) {
        fprintf(stderr, "ConnectAttach failed: errno=%d(%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    pthread_attr_init( &attr );
    pthread_attr_setschedpolicy( &attr, SCHED_RR );
    param.sched_priority = 21;
    pthread_attr_setschedparam( &attr, &param );
    pthread_attr_setinheritsched( &attr, PTHREAD_EXPLICIT_SCHED );

    char rpmsg_ctl_pathname[RPMSG_MAX_NAME_LEN];
    snprintf(rpmsg_ctl_pathname, RPMSG_MAX_NAME_LEN, "%s/ctl", rpmsg_node);

    int ctl_fd = open(rpmsg_ctl_pathname, O_RDWR);
    if (ctl_fd < 0) {
        fprintf(stderr, "Failed to open %s! errno=%d(%s)\n\n", rpmsg_ctl_pathname, errno, strerror(errno));
        return EXIT_FAILURE;
    }

    if (verbose)
        fprintf(stdout, "Open rpmsg device %s!\n", rpmsg_node);

    return ctl_fd;
}



static int rpmsg_char_write(int fd, char *buf, uint32_t len, bool complete)
{
    uint32_t written = 0;
    uint32_t ctr = 0;
    int ret;

    do {
        ret = write(fd, buf, len - written);
        if (ret < 0) {
            ret = -errno;
            fprintf(stderr, "  Failed to write to endpoint! ctr=%d, errno=%d(%s)\n\n", ctr, errno, strerror(errno));
            return ret;
        }

        written += ret;
        if (verbose > 2)
            fprintf(stdout, "  write: %d: ret=%d, %d of %d\n", ctr, ret, written, len);            

        buf += ret;
        ctr++;

    } while (complete && (written < len));

    return len;
}

static int rpmsg_char_select_read(int fd, char *buf, uint32_t len, uint32_t sub_read_len, bool complete)
{
    uint32_t read_done = 0;
    uint32_t read_len; // artificially smaller size for testing
    uint32_t ctr = 0;
    int ret;

    fd_set fds;

    FD_ZERO(&fds);
    FD_SET(fd, &fds);

    do {
        ret = select(1 + fd, &fds, 0, 0, NULL);
        if (ret < 0) {
            ret = -errno;
            fprintf(stderr, "  select() failed! ctr=%d, errno=%d(%s)\n\n", ctr, errno, strerror(errno));
            return ret;
        }

        read_len = min(sub_read_len, len - read_done);

        ret = read(fd, buf, read_len);
        if (ret < 0) {
            ret = -errno;
            fprintf(stderr, "  Failed to read from endpoint! ctr=%d, errno=%d(%s)\n\n", ctr, errno, strerror(errno));
            return ret;
        }

        read_done += ret;
        if (verbose > 2)
            fprintf(stdout, "  read: %d: requested=%d, ret=%d, %d of %d\n", ctr, read_len, ret, read_done, len);

        buf += ret;
        ctr++;
    } while (complete && (read_done < len));

    return len;
}

/* ctl_fd是通过打开/dev/rpmsg10获取的句柄， 返回ept_fd */
static int open_rpmsg_ept_fd(int ctl_fd, opts_t *opts)
{
    int ret = -1;

    char local_name[RPMSG_MAX_NAME_LEN];
    snprintf(local_name, RPMSG_MAX_NAME_LEN, "rpsample%d", opts->ept_local_addr);

    rpmsg_endpoint_attr_t eattr;
    eattr.local_name     = local_name;
    eattr.remote_name    = NULL;
    eattr.ept_class_name = opts->class_name;
    eattr.local_addr     = opts->ept_local_addr;
    eattr.remote_addr    = opts->ept_remote_addr;
    eattr.flags          = opts->ept_flags;

    ept_id = rpmsg_create_endpoint(ctl_fd, &eattr); // returns ept_id
    if (ept_id == RPMSG_INVALID_ID) {
        fprintf(stderr, "Failed to create endpoint! ept_id=%d\n\n", ept_id);
        ret = -EIO;
        return ret;
    }

    char rpmsg_ept_pathname[RPMSG_MAX_NAME_LEN];
    snprintf(rpmsg_ept_pathname, RPMSG_MAX_NAME_LEN, "%s/ept%d", opts->rpmsg_node, ept_id);

    struct sigevent sigev;
    SIGEV_PULSE_INIT(&sigev,
                     g_coid,
                     RPMSG_SAMPLE_EVENT_PRIORITY,
                     RPMSG_SAMPLE_EVENT_CODE,
                     ((opts->ept_local_addr & RPMSG_EVENT_DATA_USER_MASK) << RPMSG_EVENT_DATA_USER_SHIFT) );

    g_ept_hdl = rpmsg_open_endpoint(rpmsg_ept_pathname, O_RDWR, &sigev);
    if (g_ept_hdl == NULL) {
        fprintf(stderr, "Failed to open endpoint! %s\n\n", rpmsg_ept_pathname);
        ret = -EIO;
        return ret;
    }

    rpmsg_endpoint_prop_t prop;
    ret = rpmsg_endpoint_get_properties(g_ept_hdl, &prop);
    if (ret != EOK) {
        fprintf(stderr, "Failed to get endpoint properties! %s\n\n", rpmsg_ept_pathname);
        return ret;
    }

    int ept_fd = rpmsg_endpoint_get_fd(g_ept_hdl);
    if (ept_fd < 0) {
        fprintf(stderr, "Failed to get endpoint fd! %s\n\n", rpmsg_ept_pathname);
        ret = -EIO;
        return ret;
    }

    if (verbose)
        fprintf(stdout, "ept_hdl=0x%p path=%s fd=%d\n", g_ept_hdl, rpmsg_ept_pathname, ept_fd);

    return ept_fd;
}

static int close_rpmsg_ept_fd(int ctl_fd, int ept_fd)
{
    int ret = -1;

    if (g_ept_hdl != NULL) {
        ret = rpmsg_close_endpoint(g_ept_hdl);
        if (ret != EOK) {
            fprintf(stderr, "Failed to close endpoint! ept=%d, ept_hdl=0x%p, ret2=%d(%s)\n\n", ept_id, g_ept_hdl, ret, strerror(ret));
            //fall-through
        }
    }

    if (ept_id != RPMSG_INVALID_ID) {
        ret = rpmsg_destroy_endpoint(ctl_fd, ept_id);
        if (ret != EOK) {
            fprintf(stderr, "Failed to destroy endpoint! ept=%d, ret2=%d(%s)\n\n", ept_id, ret, strerror(ret));
            //fall-through
        }
    }

    return ret;
}


static int rpmsg_fixedLen_package(struct _rpmsg_fixedLen_message *fixedLenMsg, uint8_t *sendMsg, uint16_t len)
{
    int ret = -1;
    uint16_t crcValue = 0;
    fixedLenMsg->len = len;
    fixedLenMsg->srcId = RPMSG_SAMPLE_QNX_EPT;
    fixedLenMsg->dstId = RPMSG_SAMPLE_RTOS_EPT;

    memcpy(fixedLenMsg->msg, sendMsg, len);
    /* calculate payload crc value */
    crcValue = crc16_checksum(len, sendMsg);

    fixedLenMsg->checksum = crcValue;


    return ret;
}

static int rpmsg_fixedLen_unpackage(struct _rpmsg_fixedLen_message *fixedLenMsg, uint8_t *recvMmsg, uint16_t len)
{
    int ret = -1;
    

    return ret;
}



#if defined(__QNXNTO__) && defined(__USESRCVERSION)
#include <sys/srcversion.h>
__SRCVERSION("$URL$ $Rev$")
#endif
