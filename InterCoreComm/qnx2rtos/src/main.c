/*
 * $QNXLicenseC:
 * Copyright 2021, QNX Software Systems. All Rights Reserved.
 *
 * You must obtain a written license from and pay applicable license fees to QNX
 * Software Systems before you may reproduce, modify or distribute this
 * software, or any work that includes all or part of this software.   Free
 * development licenses are available for evaluation and non-commercial
 * purposes.  For more information visit http://licensing.qnx.com or email
 * licensing@qnx.com.
 *
 * This file may contain contributions from others.  Please review this entire
 * file for other proprietary rights or license notices, as well as the QNX
 * Development Suite License Guide at http://licensing.qnx.com/license-guide/
 * for other information.
 * $
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/slog.h>
#include <sys/slogcodes.h>
#include <pthread.h>
#include "qnx_rpmsg_intf.h"



int ipcc_fd = -1;
// char sendMsg[MAX_RPMSG_BUFF_SIZE] = "[rpmsg_demo] msg: qnx->rtos.";
// char recvMsg[MAX_RPMSG_BUFF_SIZE] = {};
char sendMsg[FIXEDLEN_MSG_PAYLOAD] = "[rpmsg_demo] msg: qnx->rtos.";
char recvMsg[FIXEDLEN_MSG_PAYLOAD] = {};


void *rpmsg_ipcc_recv_thread(void *arg);
int rpmsg_ipcc_thread_start();
int rpmsg_ipcc_loop_test();

int main(int argc, char *argv[])
{
#if 0
	int ret = -1;

	ipcc_fd = rpmsg_ipcc_fd_init();
	printf("rpmsg_ipcc_fd_init:fd %d.\n", ipcc_fd);

	if(ipcc_fd>0)
	{
		ret = rpmsg_ipcc_thread_start();
	}

	while(1)
	{
		rpmsg_ipcc_write_intf(ipcc_fd, sendMsg, strlen(sendMsg), true);
		printf("send qnx->alios msg.\n");
		sleep(1);
	}
#else
	(void )argc;
    (void )argv;
	int time = 0; /*us*/
    int opt;
	while ((opt = getopt(argc, argv, "t:s:g:f:h")) != -1) {
        switch (opt) {
            case 't':
                time = atoi(optarg);
				printf("usleep:%dus.\n", time);
                break;
            default:
                fprintf(stderr, "Invalid option.\n\n");
                break;
        }
    }
	rpmsg_ipcc_loop_test(time);
#endif
	return 0;
}



int rpmsg_ipcc_thread_start()
{
	int ret = -1;
	pthread_t ntid;
	ret = pthread_create(&ntid, NULL, rpmsg_ipcc_recv_thread, NULL);
	if (ret != 0)
	{	
		printf("can't create thread, ret:%d.", ret);
	}
	

	return ret;
}

void *rpmsg_ipcc_recv_thread(void *arg){
	char tx_buf[MAX_RPMSG_BUFF_SIZE] = {0,};
    int len = MAX_RPMSG_BUFF_SIZE;
	int recvLen = 0;
    while(1)
	{
		rpmsg_ipcc_select_read_intf(ipcc_fd, recvMsg, MAX_RPMSG_BUFF_SIZE, &recvLen, true); /* complete==true */
		printf("[rpmsg_demo] recvLen:%d, msg:%s.\n", recvLen, recvMsg);
		memset(recvMsg, 0, MAX_RPMSG_BUFF_SIZE);
	}
	return((void *)0);
}