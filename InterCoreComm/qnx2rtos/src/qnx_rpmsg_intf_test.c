#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/slog.h>
#include <sys/slogcodes.h>
#include <sys/time.h>
#include "qnx_rpmsg_intf.h"
#include "qnx_rpmsg_intf_test.h"


/* app msg struct */
#define APPDATASIZE            (473)
struct _loopTest_payload {
    uint32_t index;
    uint32_t recvIndex;
    uint8_t size;
    uint8_t arg1;
    uint8_t arg2;
    char data[APPDATASIZE];
}__attribute__((__packed__));   /*  struct size must <= 484(FIXEDLEN_MSG_PAYLOAD_SIZE), 11+473 */




int rpmsg_ipcc_loop_test(int time)
{
	int ret = -1;
    int i = 0;
    int ipcc_fd = -1;
    int recvLen = 0;

    /*time*/
    float time_use = 0;
    struct timeval start;
    struct timeval end;

    char recvMsg[MAX_RPMSG_BUFF_SIZE] = {0};
    int len = MAX_RPMSG_BUFF_SIZE;

    struct _loopTest_payload localLoopTestPayload = {};
    int lastIndex = 0;
    localLoopTestPayload.index = 0;
    localLoopTestPayload.size = APPDATASIZE;
    for(i=0; i<APPDATASIZE; i++)
    {
        localLoopTestPayload.data[i] = i;
    }

	ipcc_fd = rpmsg_ipcc_fd_init();
	printf("rpmsg_ipcc_fd_init:fd %d. sizeof(localLoopTestPayload):%d.\n", ipcc_fd, sizeof(localLoopTestPayload));

    while (1)
    {
        /* code */
        gettimeofday(&start, NULL);
        rpmsg_ipcc_write_intf(ipcc_fd, &localLoopTestPayload, sizeof(localLoopTestPayload), true);
		
        lastIndex = localLoopTestPayload.index;
        ret = rpmsg_ipcc_select_read_intf(ipcc_fd, recvMsg, MAX_RPMSG_BUFF_SIZE, &recvLen, true); /* complete==true */

        gettimeofday(&end, NULL);
        memcpy(&localLoopTestPayload, recvMsg, sizeof(localLoopTestPayload));

        time_use = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec);//微秒
        // printf("time_use is %f us, recvLen:%d. \n", time_use, recvLen);


        if(lastIndex%1000 == 1)
        {
            printf("recvIndex:%d.\n", localLoopTestPayload.recvIndex);
        }
        if(localLoopTestPayload.recvIndex == lastIndex+1)
        {
            localLoopTestPayload.index++;
            // printf("recvIndex:%d. size:%d.\n", localLoopTestPayload.recvIndex, sizeof(localLoopTestPayload));
        }
        else
        {
            printf("---------------recv error---------recvIndex:%d--------------.\n", localLoopTestPayload.recvIndex);
        }
        usleep(time);
    }
	return 0;
} 