#include "qnx_rpmsg_share_comm.h"
#include "sdp_rpmsg.h"
#include <stdio.h>
#include <stdbool.h>




#define RPMSG_MSG_BEGIN_BYTE        (0x7EU)
#define RPMSG_MSG_END_BYTE          (0x7FU)
#define RPMSG_MSG_ESCAPE_BYTE       (0x7DU)
#define RPMSG_MSG_ESCAPE_7E         (0x5EU)
#define RPMSG_MSG_ESCAPE_7F         (0x5FU)
#define RPMSG_MSG_ESCAPE_7D         (0x5DU)

typedef struct _TcpShareEscapeBuf
{
    SdpPreReadTcpMsg msgEscapeBuf;
    bool needEscape;
}TcpShareEscapeBuf;

static TcpShareEscapeBuf *tcpShareEscapeBuf_p = {0};



/* local functions */
static int share_comm_escape_encode(char *destMsgPtr, int *destLenPtr, char *srcMsgPtr, int srcLen);
static int share_comm_escape_decode(char *msgPtr, int msgLen, int comId);
static int share_comm_escape_decode_begin_deal(int tmpComId);
static int share_comm_escape_decode_end_deal(int tmpComId);
static int share_comm_escape_decode_msg_deal(int tmpComId, unsigned char msgByte);



static int share_comm_escape_encode(char *destMsgPtr, int *destLenPtr, char *srcMsgPtr, int srcLen)
{
    int ret = 0;
    int i, countIndex;

    if ((NULL == destMsgPtr) || (NULL == destLenPtr)
        || (NULL == srcMsgPtr))
    {        
        printf("share_comm_escape_encode: invalid parameters");
        ret = -1;
    }
    else
    {
        destMsgPtr[0] = (RPMSG_MSG_BEGIN_BYTE);
        countIndex = 1;
        for (int i = 0; i < srcLen; i++)
        {
            switch ((unsigned char) srcMsgPtr[i])
            {
                case RPMSG_MSG_BEGIN_BYTE:
                {
                    destMsgPtr[countIndex] = (char)RPMSG_MSG_ESCAPE_BYTE;
                    destMsgPtr[countIndex+1] = (char)RPMSG_MSG_ESCAPE_7E;
                    countIndex += 2;
                    break;
                }
                case RPMSG_MSG_END_BYTE:
                {
                    destMsgPtr[countIndex] = (char)RPMSG_MSG_ESCAPE_BYTE;
                    destMsgPtr[countIndex+1] = (char)RPMSG_MSG_ESCAPE_7F;
                    countIndex += 2;
                    break;
                }
                case RPMSG_MSG_ESCAPE_BYTE:
                {
                    destMsgPtr[countIndex] = (char)RPMSG_MSG_ESCAPE_BYTE;
                    destMsgPtr[countIndex+1] = (char)RPMSG_MSG_ESCAPE_7D;
                    countIndex += 2;     
                    break;
                }   
                
                default:
                {
                    destMsgPtr[countIndex] = srcMsgPtr[i];
                    countIndex ++;
                    break;
                }    
            }
        }
        destMsgPtr[countIndex] = RPMSG_MSG_END_BYTE;
        *destLenPtr = countIndex +1;
    }

    return ret;
}


static int share_comm_escape_decode(char *msgPtr, int msgLen, int comId)
{
    int ret = 0;
    int i = 0;

    if ((NULL == msgPtr) || (msgLen <= 0) || (msgLen >(int)492) || (comId < 1))
    {
        ILOG("share_comm_escape_decode: invalid parameters");
        ret = -1;
    }
    else
    {
        for (int i = 0; i < msgLen; i++)
        {
            if (((unsigned char)msgPtr[i] == RPMSG_MSG_BEGIN_BYTE))
            {
                share_comm_escape_decode_begin_deal(comId);
            }
            else if ((unsigned char)msgPtr[i] == RPMSG_MSG_END_BYTE)
            {
                share_comm_escape_decode_end_deal(comId);
            }
            else
            {
                share_comm_escape_decode_msg_deal(comId, (unsigned char)msgPtr[i]);
            }
        }
        
    }

    return ret;
}



static int share_comm_escape_decode_begin_deal(int tmpComId)
{
    int ret = -1;

    if (0)
    {
        ret = -1;
    }
    else
    {
        if ((0 != tcpShareEscapeBuf_p->msgEscapeBuf.msgLen)
            || (tcpShareEscapeBuf_p->needEscape == true))
        {
            ret = -1;
        }
        tcpShareEscapeBuf_p->msgEscapeBuf.msgLen = 0;
        tcpShareEscapeBuf_p->msgEscapeBuf.comId = tmpComId;
        tcpShareEscapeBuf_p->msgEscapeBuf.byteArray[0] = RPMSG_MSG_BEGIN_BYTE;
        tcpShareEscapeBuf_p->needEscape = true;
    }

    return ret;
}


static int share_comm_escape_decode_end_deal(int tmpComId)
{
    int ret = -1;

//     bool isAliveMsg = false;

//     if (0)
//     {
//         ret = -1;
//     }
//     else
//     {
//         if ((tcpShareEscapeBuf_p->msgEscapeBuf.msgLen != 0)
//             && (tcpShareEscapeBuf_p->needEscape == false))
//         {
           
//             if (isAliveMsg == false)
//             {
//                 tcpShareEscapeBuf_p->msgEscapeBuf.msgType = 21;
// 				if (tcpShareEscapeBuf_p->msgEscapeBuf.msgLen < (int)492)
// 				{
// 					if (0 != sdp_buf_mq_pre_read_udp_put_data(tcpShareQueueBuf_p, &(tcpShareEscapeBuf_p->msgEscapeBuf)))
// 					{
// 						ILOG("SHARE_TCP: Escape Buf full, comId[%d] msg discard!", tmpComId);
// 						ret = -1;
// 					}
// 				}
// 				else
// 				{
// 					ILOG("SHARE_TCP: msg len %d exceed max %d!",tcpShareEscapeBuf_p->msgEscapeBuf.msgLen,492);
// 					ret = -1;
// 				}
//             }
//         }
//         else
//         {
//             if (tcpShareEscapeBuf_p[tmpComId]->msgEscapeBuf.msgLen == 0)
//             {
// #if (TCPDEBUG > 0)
//                 DLOG("SHARE_TCP: invalid msg from [%d], 0 Lens error", tmpComId);
// #endif
//             }
//             else
//             {
// #if (TCPDEBUG > 0)
//                 DLOG("SHARE_TCP: invalid msg from [%d], Escape But END error", tmpComId);
// #endif
//             }
//             ret = -1;
//         }
//         tcpShareEscapeBuf_p[tmpComId]->msgEscapeBuf.byteArray[0] = 0;
//         tcpShareEscapeBuf_p[tmpComId]->msgEscapeBuf.comId = 0;
//         tcpShareEscapeBuf_p[tmpComId]->msgEscapeBuf.msgLen = 0;
//         tcpShareEscapeBuf_p[tmpComId]->needEscape = false;
//     }


    return ret;
}

static int share_comm_escape_decode_msg_deal(int tmpComId, unsigned char msgByte)
{
    int ret = -1;

    return ret;
}