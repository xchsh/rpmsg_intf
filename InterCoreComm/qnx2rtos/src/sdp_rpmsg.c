/******************************************************************************
  filename: sdp_tcp.c
  author: wudi
  create date: 2016-01-12
  description: this file provides  implementation of list
  warning:
  modify history:
  Name      Date        description
******************************************************************************/

/*  includes */
#include <string.h>
#include "sdp_rpmsg.h"

/* local definitions */

/* local typedefs */

/* local variables declarations */

/* local functions declarations */

/* global variables declarations */

/* function definitions */

/******************************************************************************
  function name: sdp_buf_mq_pre_read_tcp_init
  description: initialize buff used to store msg from communication layer.
  parameters: 
                    Name                IN/OUT           Type                                        Description
                    buf                     IN                 PreReadTcpBuf *                        buff pointer
  Return Value: 0
  Error Code: -1
  warning: None
******************************************************************************/
int sdp_buf_mq_pre_read_tcp_init(SdpPreReadTcpBuf *pBuf)
{
    int ret = 0;

    if (NULL == pBuf)
    {
        ret = -1;
    }
    else
    {
        memset(pBuf, 0, sizeof(SdpPreReadTcpBuf));
    }

    return (ret);
}
/******************************************************************************
  function name: sdp_buf_mq_pre_read_tcp_put_data
  description: add new msg to buff.
  parameters: 
                    Name                IN/OUT           Type                                        Description
                    pBuf                IN              SdpPreReadTcpBuf *                         buff pointer
                    pMsg                IN              SdpPreReadBufTcpMsg *                    message to add
  Return Value: 0
  Error Code: 
                    -1  
  warning: None
******************************************************************************/
int sdp_buf_mq_pre_read_tcp_put_data(SdpPreReadTcpBuf *pBuf, SdpPreReadTcpMsg *pMsg)
{
    int ret = 0;
    UINT16 tmpWriteNextIndex = 0U;

    if ((NULL == pBuf) || (NULL == pMsg))
    {
        ret = -1;
    }
    else
    {
        tmpWriteNextIndex = (pBuf->rear + 1U) % MAX_SDP_PRE_READ_TCP_SIZE;

        if (tmpWriteNextIndex != pBuf->front)
        {
            if (pBuf->rear < MAX_SDP_PRE_READ_TCP_SIZE)
            {
                memcpy(pBuf->msgArray[pBuf->rear].byteArray, pMsg->byteArray, pMsg->msgLen);
                pBuf->msgArray[pBuf->rear].msgLen = pMsg->msgLen;
                pBuf->msgArray[pBuf->rear].comId = pMsg->comId;
                pBuf->msgArray[pBuf->rear].msgType = pMsg->msgType;
                /*writeIndex added by one*/
                pBuf->rear = (pBuf->rear+1U)%MAX_SDP_PRE_READ_TCP_SIZE;
            }
            else
            {/*run exception*/
                ret = -1;
            }
        }
        else
        {/*buf is full*/
            ret = -1;
        }
    }
    
    return ret;
}
/******************************************************************************
  function name: sdp_buf_mq_pre_read_tcp_get_data
  description: get msg from buff.
  parameters: 
                    Name                IN/OUT           Type                                        Description
                    pBuf                IN              SdpPreReadTcpBuf *                          buff pointer
                    pMsg                OUT             SdpPreReadBufTcpMsg *                       message to get
  Return Value: 0
  Error Code: 
                    -1  
  warning: None
******************************************************************************/
int sdp_buf_mq_pre_read_tcp_get_data(SdpPreReadTcpBuf *pBuf, SdpPreReadTcpMsg *pMsg)
{
    int ret = 0;

    if ((NULL == pBuf) || (NULL == pMsg))
    {
        ret = -1;
    }
    else
    {
        if (pBuf->front != pBuf->rear)
        {
            if (pBuf->front < MAX_SDP_PRE_READ_TCP_SIZE)
            {
                memcpy(pMsg->byteArray, pBuf->msgArray[pBuf->front].byteArray, pBuf->msgArray[pBuf->front].msgLen);
                pMsg->msgLen = pBuf->msgArray[pBuf->front].msgLen;
                pMsg->comId = pBuf->msgArray[pBuf->front].comId;
                pMsg->msgType = pBuf->msgArray[pBuf->front].msgType;
                /*readIndex added by one*/
                pBuf->front = (pBuf->front+1U)%MAX_SDP_PRE_READ_TCP_SIZE;
            }
            else
            {/*run exception*/
                ret = -1;
            }
        }
        else
        {/*buf is empty*/
            ret = -1;
        }
    }    
    
    return ret;
}
