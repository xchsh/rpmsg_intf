/*
 * $QNXLicenseC:
 * Copyright 2021, QNX Software Systems. All Rights Reserved.
 *
 * You must obtain a written license from and pay applicable license fees to QNX
 * Software Systems before you may reproduce, modify or distribute this software,
 * or any work that includes all or part of this software.   Free development
 * licenses are available for evaluation and non-commercial purposes.  For more
 * information visit http://licensing.qnx.com or email licensing@qnx.com.
 *
 * This file may contain contributions from others.  Please review this entire
 * file for other proprietary rights or license notices, as well as the QNX
 * Development Suite License Guide at http://licensing.qnx.com/license-guide/
 * for other information.
 * $
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/procmgr.h>
#include <sys/neutrino.h>
#include <unistd.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <sys/select.h>
#include <semaphore.h>
#include <pthread.h>
#include "rpmsg/rpmsg.h"

#define RPMSG_SAMPLE_EVENT_PRIORITY (21)
#define RPMSG_SAMPLE_EVENT_CODE     (SI_NOTIFY)

/* User-defined endpoint in RTOS and QNX */
#define RPMSG_SAMPLE_RTOS_EPT  (550)
#define RPMSG_SAMPLE_QNX_EPT   (551)

/* User-defined data format */
struct _sample_payload {
    uint32_t cmd;
    uint32_t size;
    uint32_t arg1;
    uint32_t arg2;
    char data[];
}__attribute__((__packed__));

/* User-defined test command */
enum saf_cmd_type {
    CMD_REPLY_DATA = 0,
    CMD_SEND_DATA,
    CMD_CLOSE_CHANNEL,
    CMD_SEND_DATA_NOB,
};

/* User-defined test case */
enum {
    TEST_TYPE_SEND_ONLY = 1,
    TEST_TYPE_RECV_ONLY,
    TEST_TYPE_CLOSE_REMOTE,
    TEST_TYPE_NONBLOCK,
};

typedef struct {
    uint32_t  n_threads;
    uint32_t  n_data_iters;
    uint32_t  n_seconds;
    char     *rpmsg_node;
    char     *remote_name;
    uint32_t  remote_addr;
    char     *class_name;
    uint32_t  ept_local_addr;
    uint32_t  ept_remote_addr;
    uint32_t  ept_flags;
    uint32_t  test_num;
    int       verbose;
    uint32_t  sleep_ms;
} opts_t;

/* General endpoint function wrapper of open/close/read/write */
static int open_rpmsg_ctl_fd(char *rpmsg_node);
static int rpmsg_char_write(int fd, char *buf, uint32_t len, bool complete);
static int rpmsg_char_select_read(int fd, char *buf, uint32_t len, uint32_t sub_read_len, bool complete);
static int open_rpmsg_ept_fd(int ctl_fd, opts_t *opts);
static int close_rpmsg_ept_fd(int ctl_fd, int ept_fd);

static rpmsg_endpoint_t *g_ept_hdl = NULL;
static unsigned ept_id = RPMSG_INVALID_ID;
static int               g_chid;
static int               g_coid;

/* verbose:1, open/close operation, 2: rd/wr data */
static int               verbose;

/* The sample compatiable version with Linux */
#define SAMPLE_TEST_VERSION "v211015"
/* The default remote device: safety */
#define RPMSG_DEVICE_NAME   "/dev/rpmsg10"
#define RPMSG_HEADER_LEN    (16)
#define MAX_RPMSG_BUFF_SIZE (508 - RPMSG_HEADER_LEN)
#define PAYLOAD_SIZE        (MAX_RPMSG_BUFF_SIZE - sizeof(struct _sample_payload))
#define SAMPLE_SEND_TIMES   (10)

static void usage(const char *cmd)
{
    printf("This is s rpmsg sample %s to talk with safety service\n", SAMPLE_TEST_VERSION);
    printf("Run command:sspd in safety console firstly\n");

    printf("Command options:\n");
    printf("%s -h\t\t ; Show this help info\n", cmd);
    printf("%s -d [devname]\t ; Specify device name (default: %s)\n",
            cmd, RPMSG_DEVICE_NAME);
    printf("%s -v\t\t: Verbose mode\n", cmd);

    printf("\n\nFrequently used command\n");
    printf("%s -l\t\t ; to list all devices\n", cmd);
    printf("%s -s\t\t ; Send message to remote device\n", cmd);
    printf("%s -r\t\t ; Send message to remote device and wait 1 reply\n", cmd);
    printf("%s -b\t\t ; Send message to remote device and wait several continuous replies\n", cmd);
    printf("%s -c\t\t ; Close remote service\n", cmd);

    exit(0);
}

int open_rpmsg_ctl_fd(char *rpmsg_node)
{
    struct sched_param  param;
    pthread_attr_t      attr;

    // Setup the channel and connection for receiving pulse
    if ((g_chid = ChannelCreate(_NTO_CHF_PRIVATE)) < 0) {
        fprintf(stderr, "ChannelCreate failed: errno=%d(%s)\n\n", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    if ((g_coid = ConnectAttach(0, 0, g_chid, _NTO_SIDE_CHANNEL, 0)) < 0) {
        fprintf(stderr, "ConnectAttach failed: errno=%d(%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    pthread_attr_init( &attr );
    pthread_attr_setschedpolicy( &attr, SCHED_RR );
    param.sched_priority = 21;
    pthread_attr_setschedparam( &attr, &param );
    pthread_attr_setinheritsched( &attr, PTHREAD_EXPLICIT_SCHED );

    char rpmsg_ctl_pathname[RPMSG_MAX_NAME_LEN];
    snprintf(rpmsg_ctl_pathname, RPMSG_MAX_NAME_LEN, "%s/ctl", rpmsg_node);

    int ctl_fd = open(rpmsg_ctl_pathname, O_RDWR);
    if (ctl_fd < 0) {
        fprintf(stderr, "Failed to open %s! errno=%d(%s)\n\n", rpmsg_ctl_pathname, errno, strerror(errno));
        return EXIT_FAILURE;
    }

    if (verbose)
        fprintf(stdout, "Open rpmsg device %s!\n", rpmsg_node);

    return ctl_fd;
}

void list_rpmsg_device(void)
{
    system("ls -1d /dev/rpmsg*");

    exit(0);
}

// test case 0
static int send_test(int fd)
{
    int bytes_sent;
    int bytes_rcvd;
    // char tx_buf[MAX_RPMSG_BUFF_SIZE] = {0,};
    char tx_buf[MAX_RPMSG_BUFF_SIZE] = "xchshxchsh";
    char rx_buf[MAX_RPMSG_BUFF_SIZE] = {0,};
    int len = MAX_RPMSG_BUFF_SIZE;
    struct _sample_payload *tx_hdr = (struct _sample_payload *) tx_buf;
    tx_hdr->cmd = CMD_REPLY_DATA;
    tx_hdr->size  = PAYLOAD_SIZE;

    bytes_sent = rpmsg_char_write(fd, tx_buf, len, true);
    if (bytes_sent <= 0) {
        perror("\r\n Error sending data");
    }

    bytes_rcvd = rpmsg_char_select_read(fd, rx_buf, len, len, true);
    if (bytes_rcvd < len) {
        perror("\r\n Error receiving data");
    }
    else {
        printf("recieve the data %d bytes\n",bytes_rcvd);
    }

    return 0;
}

// test case 2
static int recv_test(int fd)
{
    int bytes_sent;
    int bytes_rcvd;
    char tx_buf[MAX_RPMSG_BUFF_SIZE] = {0,};
    char rx_buf[MAX_RPMSG_BUFF_SIZE] = {0,};
    int len = MAX_RPMSG_BUFF_SIZE;
    struct _sample_payload *tx_hdr = (struct _sample_payload *) tx_buf;
    struct _sample_payload *rx_hdr = (struct _sample_payload *) rx_buf;
    tx_hdr->cmd = CMD_SEND_DATA;
    tx_hdr->size  = PAYLOAD_SIZE;
    char message[] = "hello safety";
    memcpy(tx_hdr->data, message, sizeof(message));

    bytes_sent = rpmsg_char_write(fd, tx_buf, len, true);
    if (bytes_sent <= 0) {
        perror("\r\n Error sending data");
    }

    bytes_rcvd = rpmsg_char_select_read(fd, rx_buf, len, len, true);
    if (bytes_rcvd < len) {
        perror("\r\n Error receiving data");
    }
    else {
        printf("the message is: %s\n", rx_hdr->data);
    }

    return 0;
}

// test case 3
static int close_channel_test(int fd)
{
    int bytes_sent;
    char tx_buf[MAX_RPMSG_BUFF_SIZE] = {0,};
    int len = MAX_RPMSG_BUFF_SIZE;
    struct _sample_payload *tx_hdr = (struct _sample_payload *) tx_buf;
    tx_hdr->cmd = CMD_CLOSE_CHANNEL;
    tx_hdr->size  = PAYLOAD_SIZE;

    bytes_sent = rpmsg_char_write(fd, tx_buf, len, true);
    if (bytes_sent <= 0) {
        perror("\r\n Error sending data");
    }

    return 0;
}

// test case 4
static int nonblock_test(int fd)
{
    int bytes_sent;
    int bytes_rcvd;
    int recv_times;
    int rcvd_times = 0;
    int flags;
    char tx_buf[MAX_RPMSG_BUFF_SIZE] = {0,};
    char rx_buf[MAX_RPMSG_BUFF_SIZE] = {0,};
    int len = MAX_RPMSG_BUFF_SIZE;
    struct _sample_payload *tx_hdr = (struct _sample_payload *) tx_buf;
    struct _sample_payload *rx_hdr = (struct _sample_payload *) rx_buf;
    tx_hdr->cmd = CMD_SEND_DATA_NOB;
    tx_hdr->size  = PAYLOAD_SIZE;
    tx_hdr->arg2 = SAMPLE_SEND_TIMES;
    char message[] = "hello safety, send 10 messages";
    memcpy(tx_hdr->data, message, sizeof(message));

    bytes_sent = rpmsg_char_write(fd, tx_buf, len, true);
    if (bytes_sent <= 0) {
        perror("\r\n Error sending data");
    }

    if ((flags = fcntl(fd, F_GETFL, NULL)) < 0) {
        printf("fcntl(%d, F_GETFL)", fd);
        return -1;
    }
    if (!(flags & O_NONBLOCK)) {
        if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) == -1) {
            printf("fcntl(%d, F_SETFL)", fd);
            return -1;
        }
    }

    while (1) {
        bytes_rcvd = read(fd, rx_buf, len);
        if (bytes_rcvd < len) {
            continue;
        }
        else {
            recv_times = rx_hdr->arg2;
            if (rx_hdr->arg1 == 0) {
                printf("the message is: %s\n", rx_hdr->data);
            }
            printf("the data is: %d\n", rx_hdr->arg1);
            rcvd_times ++;
        }
        if (rcvd_times == recv_times) {
            break;
        }
    }

    return 0;
}

int main(int argc, char *argv[])
{
    opts_t opts = {
        .n_threads      = 1,
        .n_data_iters   = 0,
        .rpmsg_node     = RPMSG_DEVICE_NAME,
        .remote_name    = "\0",
        .remote_addr    = RPMSG_NO_ADDR,
        .class_name     = "char", /* librpmsg-ept-char.so */
        .ept_local_addr = RPMSG_SAMPLE_QNX_EPT,
        .ept_remote_addr = RPMSG_SAMPLE_RTOS_EPT,
        .ept_flags      = 0,
        .test_num       = TEST_TYPE_SEND_ONLY,
        .sleep_ms       = 0,
    };

    int opt;
    int ept_fd;

    if (argc == 1) {
        usage("rpmsg_sample");
    }

    while ((opt = getopt(argc, argv, "hlbsrcd:v:")) > 0) {
        switch (opt) {
            case 'h':
                usage("echo_test");
                break;
            case 'l':
                /* list all rmpsg node /dev/rpmsg# */
                list_rpmsg_device();
                break;
            case 'd':
                /* rmpsg node base name - /dev/rpmsg# */
                opts.rpmsg_node = strdup(optarg);
                if (opts.rpmsg_node[0] != '/') {
                    fprintf(stderr, "Invalid rpmsg node!\n\n");
                    return EXIT_FAILURE;
                }
                break;
            case 's':
                opts.test_num = TEST_TYPE_SEND_ONLY;
                break;
            case 'r':
                opts.test_num = TEST_TYPE_RECV_ONLY;
                break;
            case 'c':
                opts.test_num = TEST_TYPE_CLOSE_REMOTE;
                break;
            case 'b':
                opts.test_num = TEST_TYPE_NONBLOCK;
                break;
            case 'v':
                verbose = atoi(optarg);
                break;
            default:
                fprintf(stderr, "Invalid option.\n\n");
                return EXIT_FAILURE;
        }
    }

    if (verbose > 0) {
        (void)rpmsg_enable_client_logs(0);
    }

    int ctl_fd = open_rpmsg_ctl_fd(opts.rpmsg_node);
    if (ctl_fd < 0) {
        fprintf(stderr, "Failed to open ctl fd! errno=%d(%s)\n\n", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    if (verbose)
        fprintf(stdout, "Test Type [%d]: START\n\n", opts.test_num);

    ept_fd = open_rpmsg_ept_fd(ctl_fd, &opts);
    if (ept_fd < 0) {
        fprintf(stderr, "Failed to get endpoint fd!\n\n");
        goto done;
    }

    switch (opts.test_num)
    {
        case TEST_TYPE_SEND_ONLY:
            send_test(ept_fd);
            printf("SEND:\n");
            break;
        case TEST_TYPE_RECV_ONLY:
            recv_test(ept_fd);
            break;
        case TEST_TYPE_CLOSE_REMOTE:
            close_channel_test(ept_fd);
            break;
        case TEST_TYPE_NONBLOCK:
            nonblock_test(ept_fd);
            break;
        default:
            fprintf(stderr, "Unrecognized test type (%d)!\n\n", opts.test_num);
    }

    close_rpmsg_ept_fd(ctl_fd, ept_fd);

done:
    close(ctl_fd);

    if (verbose)
        fprintf(stdout, "Test Type [%d]: STOP\n\n", opts.test_num);

    return 0;
}

static int rpmsg_char_write(int fd, char *buf, uint32_t len, bool complete)
{
    uint32_t written = 0;
    uint32_t ctr = 0;
    int ret;

    do {
        ret = write(fd, buf, len - written);
        if (ret < 0) {
            ret = -errno;
            fprintf(stderr, "  Failed to write to endpoint! ctr=%d, errno=%d(%s)\n\n", ctr, errno, strerror(errno));
            return ret;
        }

        written += ret;
        if (verbose > 2)
            fprintf(stdout, "  write: %d: ret=%d, %d of %d\n", ctr, ret, written, len);

        buf += ret;
        ctr++;

    } while (complete && (written < len));

    return len;
}

static int rpmsg_char_select_read(int fd, char *buf, uint32_t len, uint32_t sub_read_len, bool complete)
{
    uint32_t read_done = 0;
    uint32_t read_len; // artificially smaller size for testing
    uint32_t ctr = 0;
    int ret;

    fd_set fds;

    FD_ZERO(&fds);
    FD_SET(fd, &fds);

    do {
        ret = select(1 + fd, &fds, 0, 0, NULL);
        if (ret < 0) {
            ret = -errno;
            fprintf(stderr, "  select() failed! ctr=%d, errno=%d(%s)\n\n", ctr, errno, strerror(errno));
            return ret;
        }

        read_len = min(sub_read_len, len - read_done);

        ret = read(fd, buf, read_len);
        if (ret < 0) {
            ret = -errno;
            fprintf(stderr, "  Failed to read from endpoint! ctr=%d, errno=%d(%s)\n\n", ctr, errno, strerror(errno));
            return ret;
        }

        read_done += ret;
        if (verbose > 2)
            fprintf(stdout, "  read: %d: requested=%d, ret=%d, %d of %d\n", ctr, read_len, ret, read_done, len);

        buf += ret;
        ctr++;

    } while (complete && (read_done < len));

    return len;
}

static int open_rpmsg_ept_fd(int ctl_fd, opts_t *opts)
{
    int ret = -1;

    char local_name[RPMSG_MAX_NAME_LEN];
    snprintf(local_name, RPMSG_MAX_NAME_LEN, "rpsample%d", opts->ept_local_addr);

    rpmsg_endpoint_attr_t eattr;
    eattr.local_name     = local_name;
    eattr.remote_name    = NULL;
    eattr.ept_class_name = opts->class_name;
    eattr.local_addr     = opts->ept_local_addr;
    eattr.remote_addr    = opts->ept_remote_addr;
    eattr.flags          = opts->ept_flags;

    ept_id = rpmsg_create_endpoint(ctl_fd, &eattr); // returns ept_id
    if (ept_id == RPMSG_INVALID_ID) {
        fprintf(stderr, "Failed to create endpoint! ept_id=%d\n\n", ept_id);
        ret = -EIO;
        return ret;
    }

    char rpmsg_ept_pathname[RPMSG_MAX_NAME_LEN];
    snprintf(rpmsg_ept_pathname, RPMSG_MAX_NAME_LEN, "%s/ept%d", opts->rpmsg_node, ept_id);

    struct sigevent sigev;
    SIGEV_PULSE_INIT(&sigev,
                     g_coid,
                     RPMSG_SAMPLE_EVENT_PRIORITY,
                     RPMSG_SAMPLE_EVENT_CODE,
                     ((opts->ept_local_addr & RPMSG_EVENT_DATA_USER_MASK) << RPMSG_EVENT_DATA_USER_SHIFT) );

    g_ept_hdl = rpmsg_open_endpoint(rpmsg_ept_pathname, O_RDWR, &sigev);
    if (g_ept_hdl == NULL) {
        fprintf(stderr, "Failed to open endpoint! %s\n\n", rpmsg_ept_pathname);
        ret = -EIO;
        return ret;
    }

    rpmsg_endpoint_prop_t prop;
    ret = rpmsg_endpoint_get_properties(g_ept_hdl, &prop);
    if (ret != EOK) {
        fprintf(stderr, "Failed to get endpoint properties! %s\n\n", rpmsg_ept_pathname);
        return ret;
    }

    int ept_fd = rpmsg_endpoint_get_fd(g_ept_hdl);
    if (ept_fd < 0) {
        fprintf(stderr, "Failed to get endpoint fd! %s\n\n", rpmsg_ept_pathname);
        ret = -EIO;
        return ret;
    }

    if (verbose)
        fprintf(stdout, "ept_hdl=0x%p path=%s fd=%d\n", g_ept_hdl, rpmsg_ept_pathname, ept_fd);

    return ept_fd;
}

static int close_rpmsg_ept_fd(int ctl_fd, int ept_fd)
{
    int ret = -1;

    if (g_ept_hdl != NULL) {
        ret = rpmsg_close_endpoint(g_ept_hdl);
        if (ret != EOK) {
            fprintf(stderr, "Failed to close endpoint! ept=%d, ept_hdl=0x%p, ret2=%d(%s)\n\n", ept_id, g_ept_hdl, ret, strerror(ret));
            //fall-through
        }
    }

    if (ept_id != RPMSG_INVALID_ID) {
        ret = rpmsg_destroy_endpoint(ctl_fd, ept_id);
        if (ret != EOK) {
            fprintf(stderr, "Failed to destroy endpoint! ept=%d, ret2=%d(%s)\n\n", ept_id, ret, strerror(ret));
            //fall-through
        }
    }

    return ret;
}

#if defined(__QNXNTO__) && defined(__USESRCVERSION)
#include <sys/srcversion.h>
__SRCVERSION("$URL$ $Rev$")
#endif
