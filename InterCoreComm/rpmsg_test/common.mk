# This is an automatically generated record.
# The area between QNX Internal Start and QNX Internal End is controlled by
# the QNX IDE properties.

ifndef QCONFIG
QCONFIG=qconfig.mk
endif
include $(QCONFIG)

#===== USEFILE - the file containing the usage message for the application.
USEFILE=

#===== EXTRA_INCVPATH - a space-separated list of directories to search for include files.
EXTRA_INCVPATH+= \
	$(PROJECT_ROOT)/public  \
	$(PROJECT_ROOT)/../../../include \
	$(PROJECT_ROOT)/../../../../bsp/BSP_SemiDrive_X9H_br-710_be-710_SVN946688_JBN28/install/usr/include/ \

#===== EXTRA_SRCVPATH - a space-separated list of directories to search for source files.
EXTRA_SRCVPATH+= \
	$(PROJECT_ROOT)/src 

#===== LIBS - a space-separated list of library items to be included in the link.
LIBS += rpmsg
LIBS += rpmsg-util
LIBS += sd-mailbox
LIBS += sd-mailbox-util

#===== EXTRA_LIBVPATH - a space-separated list of directories to search for library files.
EXTRA_LIBVPATH+= \
	$(QNX_TARGET)/$(CPUVARDIR)/usr/lib  \
	$(QNX_TARGET)/$(CPUVARDIR)/lib  \
	$(PROJECT_ROOT)/../../../aarch64/lib  \
	$(PROJECT_ROOT)/../../../../bsp/BSP_SemiDrive_X9H_br-710_be-710_SVN946688_JBN28/install/aarch64le/usr/lib/ \
	

#===== POST_BUILD - extra steps to do after building the image.
define POST_BUILD
-@$(CP_HOST) $(BUILDNAME) $(PROJECT_ROOT)/../../../aarch64/bin/$(BUILDNAME)
endef

include $(MKFILES_ROOT)/qmacros.mk
ifndef QNX_INTERNAL
QNX_INTERNAL=$(PROJECT_ROOT)/.qnx_internal.mk
endif
include $(QNX_INTERNAL)

postbuild:
	$(POST_BUILD)

include $(MKFILES_ROOT)/qtargets.mk

OPTIMIZE_TYPE_g=none
OPTIMIZE_TYPE=$(OPTIMIZE_TYPE_$(filter g, $(VARIANTS)))

